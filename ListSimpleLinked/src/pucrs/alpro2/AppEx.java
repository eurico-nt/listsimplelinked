package pucrs.alpro2;

public class AppEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ListSimpleLinkedEx<Integer> lista = new ListSimpleLinkedEx<>();

		for (int v = 1; v <= 10; v++)
			lista.addFirst(v);
		
		ListSimpleLinkedEx<Integer> listaCopia = (ListSimpleLinkedEx<Integer>) lista.clone();

		while (!lista.isEmpty())
			System.out.println(lista.removeFirst());
			//System.out.println(lista.removeFirst());

		if (lista.isEmpty())
			System.out.println("Lista original está vazia!");
		else {
			for (int pos = 0; pos < lista.size(); pos++)
				System.out.println(lista.get(pos));
		}
		
		if (listaCopia.isEmpty())
			System.out.println("Cópia da lista original está vazia!");
		else {
			for (int pos = 0; pos < listaCopia.size(); pos++)
				System.out.println(listaCopia.get(pos));
		}
		
		listaCopia.inverteVersao1();
		//listaCopia.inverteVersao2();
		System.out.println("Depois da inversão:");
		if (listaCopia.isEmpty())
			System.out.println("Cópia da lista original está vazia!");
		else {
			for (int pos = 0; pos < listaCopia.size(); pos++)
				System.out.println(listaCopia.get(pos));
		}
		
		ListSimpleLinkedEx<Integer> sublista = (ListSimpleLinkedEx<Integer>) listaCopia.sublist(3, 8);
		System.out.println("tamanho da sub-lista: "+sublista.size());
		for (int pos = 0; pos < sublista.size(); pos++)
			System.out.println(sublista.get(pos));
		
		listaCopia.add(5);
		System.out.println("Total de ocorrências do valor 5: "+listaCopia.contaOcorrencias(5));
		
		/*
		 * Teste de desempenho entre os métodos inverteVersao1 e inverteVersao2:
		 * O método baseado em uma lista auxiliar é muito PIOR (por causa do new).
		 */
		/*
		ListSimpleLinkedEx<Integer> lista3 = new ListSimpleLinkedEx<>();
		for(int i=0; i<1_000_000;i++)
			lista3.add(i);
		long start = System.currentTimeMillis();
		lista3.inverteVersao1();
		long end   = System.currentTimeMillis();
		System.out.println("Método 1 (array): "+(end-start));
		
		start = System.currentTimeMillis();
		lista3.inverteVersao2();
		end   = System.currentTimeMillis();
		System.out.println("Método 2 (lista): "+(end-start));
		*/
	}

}
