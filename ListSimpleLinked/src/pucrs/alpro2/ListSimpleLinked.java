package pucrs.alpro2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListSimpleLinked<E> implements ListTAD<E>, Iterable<E> {

	protected Node<E> head;
	protected Node<E> tail;
	protected int count;

	protected class Node<E> {
		public E element;
		public Node<E> next;

		public Node(E elem) {
			element = elem;
			next = null;
		}
	}

	public ListSimpleLinked() {
		head = tail = null;
		count = 0;
	}

	@Override
	public void add(E e) {
		Node<E> n = new Node<>(e);
		if (count > 0) // if (tail != null)
			tail.next = n;
		else
			head = n;
		count++;
		tail = n;
	}

	@Override
	public void add(int index, E element) {
		if (index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		Node<E> n = new Node<>(element);

		if (index == 0) // inserir ANTES do primeiro => caso especial
		{
			n.next = head;
			head = n;
		} else {
			Node<E> ant = head;
			for (int pos = 0; pos < index - 1; pos++)
				// pára um elemento antes da desejada
				ant = ant.next;

			Node<E> target = ant.next; // pega o seguinte (pos. desejada)
			ant.next = n; // aponta do anterior para o novo
			n.next = target; // aponta do novo para o seguinte
		}
		count++;
	}

	@Override
	public E get(int index) {
		if ((index < 0) || (index >= count)) {
			throw new IndexOutOfBoundsException("Pos. inválida: " + index);
		}
		Node<E> target = head;
		for (int i = 0; i < index; i++) {
			target = target.next;
		}
		return target.element;
	}

	@Override
	public int indexOf(E e) {
		Node<E> aux = head;
		int pos = 0;

		while (aux != null) {
			if (aux.element.equals(e))
				return pos;
			aux = aux.next;
		}

		return -1;
	}

	@Override
	public void set(int index, E element) {
		if ((index < 0) || (index >= count)) {
			throw new IndexOutOfBoundsException("Pos. inválida: " + index);
		}
		Node<E> target = head;
		for (int i = 0; i < index; i++) {
			target = target.next;
		}
		target.element = element;
	}

	@Override
	public boolean remove(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E remove(int index) {
		if (index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");

		if (index == 0) // removendo o primeiro?
		{
			E elem = head.element;
			head = head.next;
			count--;
			if (count == 0) // só tinha um? ou: if(head == null)
				tail = null;
			return elem;
		}
		Node<E> prev = head;
		for (int pos = 0; pos < index - 1; pos++)
			// pára um elemento antes da desejada
			prev = prev.next;

		Node<E> aux = prev.next; // pega o seguinte (pos. desejada)

		prev.next = aux.next;
		if (aux == tail) // removendo o último?
			tail = prev;

		count--;
		return aux.element;
	}

	@Override
	public boolean isEmpty() {
		return head == null;
	}

	@Override
	public int size() {
		return count;
	}

	@Override
	public boolean contains(E e) {
		return indexOf(e) != -1;
	}

	@Override
	public void clear() {
		head = tail = null;
		count = 0;
	}

	@Override
	public Iterator iterator() {
		return new Iterator<E>() {

			private Node<E> atual = head;
			
			@Override
			public boolean hasNext() {
				return atual != null;
			}

			@Override
			public E next() {
				if(atual == null)
					throw new NoSuchElementException();
				E item = atual.element;
				atual = atual.next;
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

}
