package pucrs.alpro2;

public interface ListTADEx<E> extends ListTAD<E> {

	void addFirst(E e);
	E removeFirst();
	E removeLast();
	//Object clone(); // já existe em Object, basta sobrescrever
	void inverteVersao1();
	void inverteVersao2();
	ListTADEx<E> sublist(int fromIndex, int toIndex);
	int contaOcorrencias(E element);
	void removeRepetidos();
}
