package pucrs.alpro2;

import java.util.Iterator;

public class App {

	public static void main(String[] args) {
		ListTAD<Integer> lista = new ListSimpleLinked<>();
		System.out.println("Teste de ListSimpleLinked");
		System.out.println("Tam: "+lista.size());
		lista.add(1);
		lista.add(2);
		lista.add(4);
		lista.add(5);
		lista.add(6);
		System.out.println("Tam: "+lista.size());
		lista.add(2, 3); // insere o valor 3 na pos. 2
		lista.add(0, 0); // insere o valor 0 no início
		lista.add(7);
		lista.add(8);
		lista.add(90); // teste de conflito
		
		System.out.println("Removido: "+lista.remove(8));  // apaga o 8
		System.out.println("Removido: "+lista.remove(8));  // apaga o 90
		System.out.println("Removido: "+lista.remove(0));  // apaga o primeiro elemento
		
		for(int i=0; i<lista.size(); i++)
			System.out.println(lista.get(i));
		
		ListSimpleLinked<Integer> lista2 = new ListSimpleLinked<>();
		for(int i=0; i<100000; i++)
			lista2.add(i);
		
		int valor;
		long tini = System.currentTimeMillis();
		System.out.println("Com get:");
		for(int i=0; i<lista2.size(); i++)
			valor = lista2.get(i);
		long tfim = System.currentTimeMillis();
		System.out.println("Tempo: "+(tfim-tini));

		System.out.println();
		tini = System.currentTimeMillis();
		System.out.println("Com iterador:");
		Iterator<Integer> it = lista2.iterator();
		while(it.hasNext())
			valor = it.next();
		tfim = System.currentTimeMillis();
		System.out.println("Tempo: "+(tfim-tini));

		System.out.println();
		tini = System.currentTimeMillis();
		System.out.println("Com iterador:");
		for(int valor2: lista2) { }
		tfim = System.currentTimeMillis();
		System.out.println("Tempo: "+(tfim-tini));
		
//		System.out.println("Com iterador:");
//		Iterator<Integer> it = lista.iterator();
//		while(it.hasNext())
//			System.out.println(it.next());
		
		System.out.println("Tam: "+lista.size());
		System.out.println();
		//System.out.println(lista);
	}
}
