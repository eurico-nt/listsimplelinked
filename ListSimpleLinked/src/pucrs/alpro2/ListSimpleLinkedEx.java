package pucrs.alpro2;

import java.util.NoSuchElementException;

import pucrs.alpro2.ListSimpleLinked.Node;

public class ListSimpleLinkedEx<E> extends ListSimpleLinked<E>
  implements ListTADEx<E> {

	@Override
	public void addFirst(E e) {
		Node<E> novo = new Node<E>(e);
		novo.next = head;
		head = novo;
		count++;
		if(count == 1) // primeiro nodo da lista?
			tail = head;
	}

	@Override
	public E removeFirst() {
		if(count == 0)
			throw new NoSuchElementException();
		
		E aux = head.element;
		head = head.next;
		count--;
		if(count == 0) // foi removido o último?
			tail = null;
		
		return aux;
	}

	@Override
	public E removeLast() {
		//return remove(count-1);
		if(count <= 1)
			return removeFirst();

		Node<E> prev = head;
		for (int pos = 0; pos < count-2; pos++)
			// pára um elemento antes da desejada
			prev = prev.next;

		E aux = prev.next.element; // pega o seguinte (pos. desejada)

		prev.next = null; // último, depois é sempre null
		tail = prev;

		count--;
		return aux;
	}
	
	@Override
	public Object clone()
	{
		ListSimpleLinkedEx<E> copia = new ListSimpleLinkedEx<>();
		Node<E> aux = head;
		while(aux != null)
		{
			copia.add(aux.element);
			aux = aux.next;
		}
		return copia;
	}

	@Override
	public void inverteVersao1() {
		if(count <= 1)
			return;
		// Algoritmo: cria um array auxiliar com refs para
		// os nodos da lista original
		Object[] nova = new Object[count];
		Node<E> aux = head;
		int pos = 0;
		while(aux != null) {
			nova[pos] = aux;
			pos++;
			aux = aux.next;
		}
		// Algoritmo de inversão: "caminha" até metade do array,
		// trocando as referências dos nodos
		for(pos=0; pos<count/2; pos++)
		{
			E elem = ((Node<E>)nova[pos]).element;
			((Node<E>)nova[pos]).element = ((Node<E>)nova[count-pos-1]).element;
			((Node<E>)nova[count-pos-1]).element = elem;
		}
	}

	@Override
	public void inverteVersao2() {
		if(count <= 1)
			return;
		// Algoritmo: cria uma nova lista vazia, e insere cada elemento
		// da lista original no INÍCIO dessa lista
		ListSimpleLinkedEx<E> nova = new ListSimpleLinkedEx<>();
		Node<E> aux = head;
		while(aux != null) {
			nova.addFirst(aux.element);
			aux = aux.next;
		}
		// No final, aponta o head e tail da lista original para ela
		// (efetivamente, destrói os nodos da lista original)
		head = nova.head;
		tail = nova.tail;
	}

	@Override
	public ListTADEx<E> sublist(int fromIndex, int toIndex) {
		if(fromIndex < 0 || toIndex > size() || fromIndex > toIndex)
			throw new IndexOutOfBoundsException("Intervalo inválido");
		
		ListSimpleLinkedEx<E> nova = new ListSimpleLinkedEx<>();
		
		Node<E> aux = head;
		for (int i = 0; i < fromIndex; i++) {
			aux = aux.next;
		}
		
		for (int i = fromIndex; i < toIndex; i++) {
			nova.add(aux.element);
			aux = aux.next;
		}
		
		return nova;
	}

	@Override
	public int contaOcorrencias(E element) {
		int total = 0;
		Node<E> aux = head;
		//for(int i=0; i<size(); i++)
		while(aux != null) {
			if(aux.element.equals(element))
				total++; // achei mais um
			aux = aux.next;
		}
		return total;
	}

	@Override
	// Algoritmo MUITO INEFICIENTE para remoção de repetidos
	// Tema de casa: melhorar!
	// Dica: esses gets têm que sumir!
	public void removeRepetidos() {
		for(int pos1=0; pos1 < count-1; pos1++) {
			E elem = get(pos1);
			for(int pos2=pos1+1; pos2 < count; pos2++) {
				E elem2 = get(pos2);
				if(elem.equals(elem2)) {
					remove(pos2);
					pos2--; // volta um para o for não avançar
				}					
			}
		}
	}
	
	
	
	
	
	
	
	
}
